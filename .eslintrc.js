module.exports = {
    "root": true,
        "env": {
        "node": true
    },
    "extends": [
        "plugin:vue/vue3-essential",
        "@vue/airbnb",
        "@vue/typescript/recommended"
    ],
        "parserOptions": {
        "ecmaVersion": 2020
    },
    "rules": {
        'no-console': 'off',
        'no-debugger': 'off',
        'import/no-cycle': 'off',
        'import/prefer-default-export': 'off',
        'no-shadow': 'off',
        "arrow-parens": "off",
        "max-len": ["warn", 200],
        'prefer-rest-params': 'off',
        'no-undef': 'warn',
        'func-names': 'off',
        'consistent-return': 'off',
        'no-plusplus': 'off',
        'no-continue': 'off',
        'no-return-assign': 'off',
        'no-useless-escape': 'off',
        'no-bitwise': 'off',
        'no-param-reassign': 'off',
        'object-curly-newline': 'off',
    }
}
