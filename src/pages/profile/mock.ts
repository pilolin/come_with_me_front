// ts-ignore
export default {
  profile: {
    name: 'Ави Рохан',
    age: 19,
    avatar: 'https://ltdfoto.ru/images/profile-1.jpg',
    work: 'Наследие 21 | стартап школа, Дизайнер',
    education: 'СПГХПА им. А.Л.Штиглица',
  },
  description: `
    <p>
      Создание новой глобальной образовательной системы Построение РОЭ (ресурсо-ориентированной экономики)
    </p>
    <ul>
      <li>
        -UX/UI дизайнер, графический дизайнер, иллюстратор. Создаю интуитивно понятные интерфейсы.
      </li>
      <li>
        -Создала более 15 сайтов под ключ.
      </li>
      <li>
        -Участвую в разработке 4 приложений.
      </li>
      <li>
        -Есть опыт в разработке фирменного стиля для инновационных товаров и продуктов в онлайн среде.
      </li>
      <li>
        -Разрабатываю баннеры, плакаты и прочие полиграфические продукты.
      </li>
    </ul>
    <p>
      Работаю в программах:<br>
      Adobe Photoshop, Adobe illustrator, Adobe After Effects, Adobe Premiere Pro, Figma, Tilda.
    </p>
  `,
  intereses: [
    '<i class="icon-ico-service-54"></i> IT',
    '<i class="icon-ico-service-47"></i> Психология',
    '<i class="icon-ico-service-32"></i> Прогулки',
  ],
  gallery: [
    { src: 'https://ltdfoto.ru/images/profile-2.jpg', alt: '123' },
    { src: 'https://ltdfoto.ru/images/profile-3.jpg', alt: '123' },
    { src: 'https://ltdfoto.ru/images/profile-4.jpg', alt: '123' },
  ],
};
