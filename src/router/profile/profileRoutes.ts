import { profileNamesRoutes } from '@/router/profile/types';
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/profile',
    name: profileNamesRoutes.PROFILE,
    component: () => import(/* webpackChunkName: "profile" */ '@/pages/profile/Profile.vue'),
    meta: { requiresAuth: true },
  },
];

export default routes;
