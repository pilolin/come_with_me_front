import { calendarNamesRoutes } from '@/router/calendar/types';
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/calendar',
    name: calendarNamesRoutes.CALENDAR,
    component: () => import(/* webpackChunkName: "calendar" */ '@/pages/Calendar.vue'),
    meta: { requiresAuth: true },
  },
];

export default routes;
