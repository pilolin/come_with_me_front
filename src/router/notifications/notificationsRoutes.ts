import { notificationsNamesRoutes } from '@/router/notifications/types';
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/notifications',
    name: notificationsNamesRoutes.NOTIFICATIONS,
    component: () => import(/* webpackChunkName: "notifications" */ '@/pages/Notifications.vue'),
    meta: { requiresAuth: true },
  },
];

export default routes;
