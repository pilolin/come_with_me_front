import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { namesRoutes } from '@/router/types';
import store from '@/store';
import messagesRoutes from '@/router/messages/messagesRoutes';
import profileRoutes from '@/router/profile/profileRoutes';
import eventsRoutes from '@/router/events/eventsRoutes';
import notificationsRoutes from '@/router/notifications/notificationsRoutes';
import calendarRoutes from '@/router/calendar/calendarRoutes';

const routes: RouteRecordRaw[] = [
  ...messagesRoutes,
  ...profileRoutes,
  ...notificationsRoutes,
  ...calendarRoutes,
  ...eventsRoutes,
  {
    path: '/register',
    name: namesRoutes.REGISTER,
    component: () => import(/* webpackChunkName: "main" */ '@/pages/login/Register.vue'),
    meta: { guest: true },
  },
  {
    path: '/',
    name: namesRoutes.LOGIN,
    component: () => import(/* webpackChunkName: "main" */ '@/pages/login/Login.vue'),
    meta: { guest: true },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  scrollBehavior() {
    return { top: 0 };
  },
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.isAuth) {
    next({ name: namesRoutes.LOGIN, params: { nextUrl: to.fullPath } });

    return;
  }

  next();
});

export default router;
