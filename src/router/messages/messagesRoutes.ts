import { messagesNamesRoutes } from '@/router/messages/types';
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/messages',
    name: messagesNamesRoutes.MESSAGES,
    component: () => import(/* webpackChunkName: "messages" */ '@/pages/Messages.vue'),
    meta: { requiresAuth: true },
  },
];

export default routes;
