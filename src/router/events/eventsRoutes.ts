import { eventsNamesRoutes } from '@/router/events/types';
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/events',
    name: eventsNamesRoutes.EVENTS,
    component: () => import(/* webpackChunkName: "events" */ '@/pages/Events.vue'),
    meta: { requiresAuth: true },
  },
];

export default routes;
