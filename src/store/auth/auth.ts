import types from '@/store/auth/types';
import { ICredential } from '@/models/ICredential';

const state = {
  isAuth: false,
};

const mutations = {
  setAuth(state, isAuth) {
    state.isAuth = isAuth;
  },
};

const actions = {
  [types.LOGIN]: async ({ commit }, credential: ICredential) => {
    await commit('setAuth', true);
    console.log(credential);
  },
  [types.LOGOUT]: ({ commit }) => {
    commit('setAuth', false);
  },
};

const getters = {
  isAuth: (state) => state.isAuth,
};

export default {
  state,
  mutations,
  actions,
  getters,
};
